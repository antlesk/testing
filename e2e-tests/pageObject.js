let pageObject = function() {
    let name = element(by.name('identifier')),
    password = element(by.name('password')),
    home_page = element(by.className('row home gtm-cloak-grey gtm-cloaked gtm-cloaked-spinner gtm-cloaked-ready')),
    createAccount = element(by.className('image gtm-account-settings-icon')),
    accountName = element(by.model('ctrl.form.account.data.name')),
    countries = element(by.model('ctrl.form.country')),
    allCountries = "/html/body/div[1]/div/div[2]/gtm-account-provision/gtm-admin-page/div/div/div[2]/div/div/div/" +
        "content/form/div[1]/div/gtm-account-form/div/div[2]/select/",
    belarus = element(by.cssContainingText('option', 'Беларусь')),
    checkBox = element(by.model('ctrl.form.account.data.shareData')),
    container = element(by.model('ctrl.form.container.data.name')),
    iOS = element(by.xpath('/html/body/div[1]/div/div[2]/gtm-account-provision/gtm-admin-page/div/div/div[2]/div/div/' +
        'div/content/form/div[3]/gtm-container-form/div/div[2]/div/div[2]')),
    EC = protractor.ExpectedConditions;

    this.authorization = function (){
        browser.get('https://tagmanager.google.com/#/admin/accounts/create');
        name.sendKeys('antlon.lesk@gmail.com\n');
        browser.wait(EC.visibilityOf(password), 5000);
        password.sendKeys('123anton789\n');
        browser.wait(EC.visibilityOf(home_page), 20000)
            .then(() => browser.sleep(1000))
            .then(() => createAccount.click());
    };

    this.enterAccountName = function (){
        browser.wait(EC.visibilityOf(accountName), 10000);
        accountName.sendKeys('demo');
        return accountName.getAttribute('value');
    };

    this.chooseCountry = function (){
        countries.click();
        browser.wait(EC.visibilityOf(belarus), 10000);
        belarus.click();

        let res = countries.getAttribute('value')
            .then(function (value) {
                return element(by.xpath(`${allCountries}option[@value="${value}"]`)).getText();
            });

        return res;
    };

    this.chooseCheckBox = function (){
        checkBox.click();

        return checkBox.getAttribute('class');
    };

    this.enterContainer = function (){
        container.sendKeys('result');

        return container.getAttribute('value');
    };

    this.chooseIOS = function (){
        iOS.click();
        return iOS.getAttribute('class');
    };
};

module.exports = new pageObject();